FROM openjdk:8-jdk-alpine
ARG JAR_FILE
ADD target/${JAR_FILE} /usr/share/hellok/hellok.jar
ADD run.sh run.sh
RUN chmod +x run.sh
CMD ./run.sh
EXPOSE 8080
