package br.com.leandroferreira.hellok

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
class HellokApplication

fun main(args: Array<String>) {
    runApplication<HellokApplication>(*args)
}

@RestController
class DefaultController {

    @RequestMapping("/hi")
    fun sayHello(): ResponseEntity<String> = ResponseEntity("It works!", HttpStatus.OK)
}